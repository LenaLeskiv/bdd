const BaseComponent = require('./base.component');

class TopNavigationComponent extends BaseComponent{

    constructor() {
        //super('.top-navigation-ui-23');
        super('.top-navigation__row');
    }
       
    item(param){
        const selectors = {
            services: '.js-op[href="/services"]',
            industries: '//span[text()="Industries"]',
            insights: '[href="/insights"]',
            about: '.js-op[href="/about"]',
            careers: '.js-op[href="/careers"]'
        };
        return this.rootEl.$(selectors[param]);
    }

    element(attribute){
        const selectors = {
            services: 'li:nth-child(1).top-navigation__item',
            industries: 'li:nth-child(2).top-navigation__item',
            insights: 'li:nth-child(3).top-navigation__item',
            about: 'li:nth-child(4).top-navigation__item',
            careers: 'li:nth-child(5).top-navigation__item'
        };
        return this.rootEl.$(selectors[attribute]);
    }
     
}
module.exports = TopNavigationComponent;