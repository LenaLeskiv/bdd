@Main2
Feature: Main2

@3
Scenario: Main page should have "EPAM | Software Engineering & Product Development Services" title3
    Given I open "Main" page
    Then Page title should "be equal to" "EPAM | Software Engineering & Product Development Services"

@4
Scenario: Main page should have "EPAM | Software Engineering & Product Development Services" title4
    Given I open "Main" page
    Then Page title should "not be equal to" "EPAM | Software Engineering & Product Developments Services"