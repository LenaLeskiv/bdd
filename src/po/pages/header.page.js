const { TopNavigationComponent } = require('../components');
const HeaderComponent = require('./../components/common/header.component');

class HeaderPage{
    
    constructor() {
        this.headerMenu = new HeaderComponent();
        this.topNavigation = new TopNavigationComponent();
    }
    
}
module.exports = HeaderPage;