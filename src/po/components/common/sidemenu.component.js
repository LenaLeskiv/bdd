const BaseComponent = require('./base.component');

class SideMenuComponent extends BaseComponent{

    constructor() {
        super('.os-content');
    }

    //get rootEl(){
       //return $('.os-content'); //'.hamburger-menu-ui'
    //}

    item(param){
        const selectors = {
            services: '[href="/services"]',
            industries: '[gradient-text="Industries"]>div',
            insights: '[href="/insights"]',
            about: '[href="/about"]',
            careers: '[href="/careers"]',
            contact: '.cta-button-ui'
        };
        return this.rootEl.$(selectors[param]);
    }
}

module.exports = SideMenuComponent;