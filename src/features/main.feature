Feature: Main

@1
Scenario: Main page should have "EPAM | Software Engineering & Product Development Services" title1
    Given I open "Main" page
    Then Page title should "be equal to" "EPAM | Software Engineering & Product Development Services"

@2
Scenario: Main page should have "EPAM | Software Engineering & Product Development Services" title2
    Given I open "Main" page
    Then Page title should "not be equal to" "EPAM | Software Engineering & Product Developments Services"

Scenario: The left sidebar should be opened and all sections should be clickable
    Given I open the "Main EPAM" page
    When I click the left sidebar
    And I click the all sections in opened drop-down menu step by step: "Services", "Industries", "Insights", "About", "Careers"
    Then The all sections were opened separately