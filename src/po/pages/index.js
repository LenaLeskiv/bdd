const HeaderPage = require('./header.page');
const MainPage = require('./main.page');

/**
 * 
  @param name {'main' | 'header'} 
  @returns {'MainPage' | 'HeaderPage'}
 */

function pages(name) {
   const items = {
    main: new MainPage(),
    header: new HeaderPage()
   }
   return items[name];
}

module.exports = {
    HeaderPage,
    MainPage,
    pages,
}