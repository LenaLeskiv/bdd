const BasePage = require('./base.page');


class MainPage extends BasePage{
    
    constructor() {
        super('https://www.epam.com/')
    }
    
    // async open() {
    //     await browser.url("https://www.epam.com/");
    // }
}
module.exports = MainPage;